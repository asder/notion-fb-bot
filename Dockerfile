FROM node:10

RUN mkdir -p /usr/src/app/

COPY ./package*.json /usr/src/app/

RUN cd /usr/src/app && npm install --production

WORKDIR /usr/src/app

COPY . /usr/src/app

CMD [ "npm", "start" ]
