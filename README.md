This project was bootstrapped with
[Bottender](https://github.com/Yoctol/bottender) init script.

## Configuration for development

```sh
npm install
```

To make the bot work, you must put environment variables into your `.env` file.

```sh
MESSENGER_PAGE_ID=[MESSENGER_PAGE_ID]
MESSENGER_ACCESS_TOKEN=[MESSENGER_ACCESS_TOKEN]
MESSENGER_APP_ID=[MESSENGER_APP_ID]
MESSENGER_APP_SECRET=[MESSENGER_APP_SECRET]
MESSENGER_VERIFY_TOKEN=[MESSENGER_VERIFY_TOKEN]
```




To set up ngrok for your server, run:
```sh
npx ngrok http 5000
```
You will get a webhook URL in the format of https://xxxxxxxx.ngrok.io from your terminal.

### Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in development mode.<br>
The bot will automatically reload if you make changes to the code.<br>
By default, server runs on [http://localhost:5000](http://localhost:5000).

To run in [Console Mode](https://bottender.js.org/docs/en/the-basics-console-mode)

```sh
npm run console
```

### `npm test`

Runs the test cases using [Jest](https://jestjs.io/).


## Configuration for production

```sh
npm install --production
```

Set environment variables

```sh
PORT=[PORT]
WEBHOOK_URL=[WEBHOOK_URL]
MESSENGER_PAGE_ID=[MESSENGER_PAGE_ID]
MESSENGER_ACCESS_TOKEN=[MESSENGER_ACCESS_TOKEN]
MESSENGER_APP_ID=[MESSENGER_APP_ID]
MESSENGER_APP_SECRET=[MESSENGER_APP_SECRET]
MESSENGER_VERIFY_TOKEN=[MESSENGER_VERIFY_TOKEN]
```

### `npm start`

Runs the app in production mode.<br>
By default, server runs on [http://localhost:5000](http://localhost:5000).

## Build and run Docker image 
Build
```sh
docker build -t [DOCKER_IMAGE_NAME] .
```
Run
```sh
docker run --env PORT=[PORT] --env MESSENGER_PAGE_ID=[MESSENGER_PAGE_ID] --env MESSENGER_ACCESS_TOKEN=[MESSENGER_ACCESS_TOKEN] --env MESSENGER_APP_ID=[MESSENGER_APP_ID] --env MESSENGER_APP_SECRET=[MESSENGER_APP_SECRET] --env MESSENGER_VERIFY_TOKEN=[MESSENGER_VERIFY_TOKEN] -p [PUBLIC_PORT]:[PORT] --expose=[PORT] -d [DOCKER_IMAGE_NAME]
```

Server will run on `http://localhost:[PUBLIC_PORT]`
