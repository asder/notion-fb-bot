jest.mock('./steps');
jest.mock('./messages-service');
const {scenario, scenarioRunner} = require('./scenario');
const steps = require('./steps');
const {messagesService} = require('./messages-service');

describe('scenario.js', () => {
    let contextMock;
    beforeEach(() => {
        contextMock = {
            event: {},
            state: {
                step: 0
            },
            sendText: jest.fn(async (text) => Promise.resolve()),
            setState: obj => Object.assign(contextMock.state, obj),
        };
    });

    describe('when recieved not text', () => {
        it('should use useTextMessage step', async () => {
            await scenarioRunner(contextMock);
            expect(steps.useTextMessage).toHaveBeenCalled();
        });
    });

    describe('when recieved text', () => {
        beforeEach(() => {
            contextMock.event.isText = true;
            contextMock.event.text = 'mockText';
            scenario[0] = jest.fn(async (text) => Promise.resolve());
        });

        it('should add text to message storage', async () => {
            await scenarioRunner(contextMock);
            expect(messagesService.add).toHaveBeenCalledWith('mockText');
        });
        it('should run scenario step', async () => {
            await scenarioRunner(contextMock);
            expect(scenario[0]).toHaveBeenCalled();
        });
    });
});