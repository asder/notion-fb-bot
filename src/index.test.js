const App = require('.');
jest.mock('./scenario');
const {scenarioRunner} = require('./scenario');

describe('index.js', () => {
    it('should be defined', () => {
        expect(App).toBeDefined();
    });
    it('should call scenario', async() => {
        await App({});
        expect(scenarioRunner).toHaveBeenCalled();
    });
});
