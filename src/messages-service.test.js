const {MessagesService} = require('./messages-service');

describe('messages-service.js', () => {
    let messagesService;
    beforeEach(() => {
        messagesService = new MessagesService();
    });

    describe('list:', () => {
        it('should get messages from service storage', () => {
            messagesService.add('someText1');
            messagesService.add('someText2');
            messagesService.add('someText3');
            const messages = messagesService.list();
            expect(messages).toEqual([{id: 0, text: 'someText1'}, {id: 1, text: 'someText2'}, {id: 2, text: 'someText3'}]);
        });
    });

    describe('add:', () => {
        it('should add message to the service storage', () => {
            messagesService.add('someText');
            const messages = messagesService.list();
            expect(messages).toEqual([{id: 0, text: 'someText'}]);
        });
        it('should return last id', () => {
            expect(messagesService.add('someText')).toBe(0);
        });
    });

    describe('get:', () => {
        it('should get message to the service storage by id', () => {
            messagesService.add('someText');
            const id = messagesService.add('someOtherText');
            const message = messagesService.get(id);
            expect(message).toEqual({id: 1, text: 'someOtherText'});
        });

        describe('when message is not found', () => {
            it('should return false', () => {
                expect(messagesService.get(4324)).toBe(false);
            });
        });
    });

    describe('remove:', () => {
        it('should remove message from the service storage by id', () => {
            messagesService.add('someText');
            const id = messagesService.add('someOtherText');
            messagesService.remove(id);
            const messages = messagesService.list();
            expect(messages).toEqual([{id: 0, text: 'someText'}]);
        });

        describe('when message is not found', () => {
            it('should return false', () => {
                expect(messagesService.get(4324)).toBe(false);
            });
        });
    });
});