const {scenarioRunner} = require('./scenario');

module.exports = async function App(context) {
    try {
        await scenarioRunner(context);
    } catch(e) {
        console.log(e);
    }
};
