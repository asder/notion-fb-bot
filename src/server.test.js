jest.mock('./messages-service');
const request = require('supertest');
const {createServer} = require('./server');

Object.assign(process.env, {
    MESSENGER_PAGE_ID: 'MESSENGER_PAGE_ID',
    MESSENGER_ACCESS_TOKEN: 'MESSENGER_ACCESS_TOKEN',
    MESSENGER_APP_ID: 'MESSENGER_APP_ID',
    MESSENGER_APP_SECRET: 'MESSENGER_APP_SECRET',
    MESSENGER_VERIFY_TOKEN: 'MESSENGER_VERIFY_TOKEN',
});

describe('server.js', () => {
    let server;
    beforeEach(async(done) => {
        server = await createServer();
        done();
    });
    afterEach((done) => {
        server.close(done);
    });

    it('responds to get /messages route', async () => {
        const res = await request(server).get('/messages');
        expect(res.status).toBe(200);
    });

    it('responds to get /messages/:id route', async () => {
        const res = await request(server).get('/messages/1');
        expect(res.status).toBe(404);
    });

    it('responds to delete /messages/:id route', async () => {
        const res = await request(server).delete('/messages/1');
        expect(res.status).toBe(404);
    });
});