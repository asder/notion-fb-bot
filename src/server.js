const bodyParser = require('body-parser');
const express = require('express');
const { bottender } = require('bottender');
const { messagesService } = require('./messages-service');

const port = Number(process.env.PORT) || 5000;
const webhookUrl = process.env.NODE_ENV === 'production' ? process.env.WEBHOOK_URL : process.env.WEBHOOK_URL || '*';

const createServer = async () => {
    const app = bottender({
        dev: process.env.NODE_ENV !== 'production',
    });

    // the request handler of the bottender app
    const handle = app.getRequestHandler();

    await app.prepare();
    const server = express();

    const verify = (req, _, buf) => {
        req.rawBody = buf.toString();
    };
    server.use(bodyParser.json({ verify }));
    server.use(bodyParser.urlencoded({ extended: false, verify }));

    server.get('/messages', (req, res) => {
        const messages = messagesService.list();
        res.json({messages});
    });

    server.get('/messages/:id', (req, res) => {
        const message = messagesService.get(+req.params.id);
        if (!message) {
            return res.status(404) && res.end();
        }
        res.json({message});
    });

    server.delete('/messages/:id', (req, res) => {
        if (!messagesService.remove(+req.params.id)) {
            res.status(404);
        }
        res.end();
    });

    // route for webhook request
    server.all(webhookUrl, (req, res) => {
        return handle(req, res);
    });

    return new Promise((resolve, reject) => {
        const instance = server.listen(port, err => {
            if (err) {
                return reject(err);
            }
            console.log(`> Ready on http://localhost:${port}`);
            resolve(instance);
        });
    });
};

process.env.NODE_ENV !== 'test' && createServer();

module.exports = {
    createServer
};