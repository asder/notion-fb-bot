class MessagesService {
    #messages
    #lastId
    constructor() {
        this.#messages = new Map();
        this.#lastId = -1;
    }

    add(text) {
        this.#messages.set(++this.#lastId, text);
        return this.#lastId;
    }

    get(id) {
        if(!this.#messages.has(id)) {
            return false;
        }
        return {id, text: this.#messages.get(id)};
    }

    remove(id) {
        if(!this.#messages.has(id)) {
            return false;
        }
        this.#messages.delete(id);
        return true;
    }

    list() {
        return Array.from(this.#messages).map(([id, text]) => ({id, text}));
    }
}

const messagesService = new MessagesService();

module.exports = {
    messagesService,
    MessagesService
}