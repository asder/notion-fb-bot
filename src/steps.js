const moment = require('moment');

async function useTextMessage(context) {
    await context.sendText('Please use text messages');
}

async function repeat(context) {
    if (context.state.requiredAnswer && context.event.text.trim().match(/^y/i)) {
        context.setState({
            usersName: undefined,
            birthDate: undefined,
            step: 0,
            requiredAnswer: false,
        });
    } else {
        await context.sendText('Do you want to repeat conversation?');
        context.setState({
            requiredAnswer: true,
        });
    }
}

async function usersName(context) {
    if (context.state.requiredAnswer) {
        const usersName = context.event.text.trim();
        if (!usersName.match(/^\w+$/i)) {
            await context.sendText('Please use correct name');
        } else {
            context.setState({
                usersName,
                step: ++context.state.step,
                requiredAnswer: false,
            });
        }
    } else {
        await context.sendText('What is your first name?');
        context.setState({
            requiredAnswer: true,
        });
    }
}

async function birthdate(context) {
    if (context.state.requiredAnswer) {
        const birthDate = moment(context.event.text, 'YYYY-MM-DD');
        if (!birthDate.isValid()) {
            await context.sendText('Please use YYYY-MM-DD format');
        } else {
            context.setState({
                requiredAnswer: false,
                birthDate,
                step: ++context.state.step,
            });
        }
    } else {
        await context.sendText('What is Your birth date?');
        context.setState({
            requiredAnswer: true,
        });
    }
}

async function daysTillNextBirthdate(context) {
    if (context.state.requiredAnswer) {
        if (context.event.text.trim().match(/^y/i)) {
            const now = moment();
            let closestBirthdate = moment(context.state.birthDate).set('year', now.year());
            if (now > closestBirthdate) {
                closestBirthdate.set('year', now.year() + 1);
            }
            const daysToBithday = closestBirthdate.diff(now, 'days');

            await context.sendText(`There are ${daysToBithday} days left until your next birthday`);
        }
        
        await context.sendText('Goodbye👋');
        
        context.setState({
            requiredAnswer: false,
            step: ++context.state.step,
        });
    } else {
        await context.sendText('Do you want to know how many days till your next birtday?');
        context.setState({
            requiredAnswer: true,
        });
    }
}

module.exports = {
    useTextMessage,
    usersName,
    birthdate,
    daysTillNextBirthdate,
    repeat,
};