const steps = require('./steps');
const moment = require('moment');

describe('steps.js', () => {
    let contextMock;
    beforeEach(() => {
        contextMock = {
            sendText: jest.fn(async (text) => Promise.resolve()),
            state: {
                step: Number(Math.random().toString().slice(2))
            },
            setState: obj => Object.assign(contextMock.state, obj),
            event: {},
        };
    });

    describe('useTextMessage:', () => {
        it('should send "Please use text messages"', async () => {
            await steps.useTextMessage(contextMock);
            expect(contextMock.sendText).toHaveBeenCalledWith('Please use text messages');
        });
    });

    describe('repeat:', () => {
        it('should send "Do you want to repeat conversation?"', async () => {
            await steps.repeat(contextMock);
            expect(contextMock.sendText).toHaveBeenCalledWith('Do you want to repeat conversation?');
        });
        it('should set state to require answer', async () => {
            await steps.repeat(contextMock);
            expect(contextMock.state.requiredAnswer).toBe(true);
        });

        describe('when answer required:', () => {
            beforeEach(() => {
                contextMock.state.requiredAnswer = true;
            });

            describe('when answer match /y/', () => {
                beforeEach(() => {
                    contextMock.event.text = 'yes';
                });

                it('should clean state and set step to 0', async () => {
                    await steps.repeat(contextMock);
                    expect(contextMock.state).toEqual({
                        usersName: undefined,
                        birthDate: undefined,
                        step: 0,
                        requiredAnswer: false,
                    });
                });
            });

            describe('otherwise', () => {
                beforeEach(() => {
                    contextMock.event.text = 'no';
                });

                it('should send "Do you want to repeat conversation?"', async () => {
                    await steps.repeat(contextMock);
                    expect(contextMock.sendText).toHaveBeenCalledWith('Do you want to repeat conversation?');
                });
                it('should set state to require answer', async () => {
                    await steps.repeat(contextMock);
                    expect(contextMock.state.requiredAnswer).toBe(true);
                });
            });
        });
    });

    describe('usersName:', () => {
        it('should send "What is your first name?"', async () => {
            await steps.usersName(contextMock);
            expect(contextMock.sendText).toHaveBeenCalledWith('What is your first name?');
        });
        it('should set state to require answer', async () => {
            await steps.usersName(contextMock);
            expect(contextMock.state.requiredAnswer).toBe(true);
        });

        describe('when answer required:', () => {
            beforeEach(() => {
                contextMock.state.requiredAnswer = true;
            });

            describe('when answer is correct', () => {
                beforeEach(() => {
                    contextMock.event.text = 'John';
                });

                it('should set name, next step and clean requiredAnswer', async () => {
                    const stepBefore = contextMock.state.step;
                    await steps.usersName(contextMock);
                    expect(contextMock.state).toEqual({
                        usersName: 'John',
                        step: stepBefore + 1,
                        requiredAnswer: false,
                    });
                });
            });

            describe('otherwise', () => {
                beforeEach(() => {
                    contextMock.event.text = 'sadas sadas';
                });

                it('should send "Please use correct name"', async () => {
                    await steps.usersName(contextMock);
                    expect(contextMock.sendText).toHaveBeenCalledWith('Please use correct name');
                });
            });
        });
    });

    describe('birthdate:', () => {
        it('should send "What is Your birth date?"', async () => {
            await steps.birthdate(contextMock);
            expect(contextMock.sendText).toHaveBeenCalledWith('What is Your birth date?');
        });
        it('should set state to require answer', async () => {
            await steps.birthdate(contextMock);
            expect(contextMock.state.requiredAnswer).toBe(true);
        });

        describe('when answer required:', () => {
            beforeEach(() => {
                contextMock.state.requiredAnswer = true;
            });

            describe('when answer is correct', () => {
                beforeEach(() => {
                    contextMock.event.text = '1988-07-20';
                });

                it('should set birthDate, next step and clean requiredAnswer', async () => {
                    const stepBefore = contextMock.state.step;
                    await steps.birthdate(contextMock);
                    expect(contextMock.state).toEqual({
                        birthDate: expect.any(Object),
                        step: stepBefore + 1,
                        requiredAnswer: false,
                    });
                });
            });

            describe('otherwise', () => {
                beforeEach(() => {
                    contextMock.event.text = 'sadassadas';
                });

                it('should send "Please use YYYY-MM-DD format"', async () => {
                    await steps.birthdate(contextMock);
                    expect(contextMock.sendText).toHaveBeenCalledWith('Please use YYYY-MM-DD format');
                });
            });
        });
    });

    describe('daysTillNextBirthdate:', () => {
        it('should send "Do you want to know how many days till your next birtday?"', async () => {
            await steps.daysTillNextBirthdate(contextMock);
            expect(contextMock.sendText).toHaveBeenCalledWith('Do you want to know how many days till your next birtday?');
        });
        it('should set state to require answer', async () => {
            await steps.daysTillNextBirthdate(contextMock);
            expect(contextMock.state.requiredAnswer).toBe(true);
        });

        describe('when answer required:', () => {
            beforeEach(() => {
                contextMock.state.requiredAnswer = true;
            });

            describe('when answer match /y/', () => {
                beforeEach(() => {
                    contextMock.event.text = 'y';
                    contextMock.state.birthDate = moment('1988-07-20');
                });

                it('should send "There are N days left until your next birthday"', async () => {
                    await steps.daysTillNextBirthdate(contextMock);
                    expect(contextMock.sendText).toHaveBeenCalled();
                    expect(contextMock.sendText.mock.calls[0][0]).toMatch(/^There are \d+ days left until your next birthday$/);
                });
                it('should send "Goodbye👋"', async () => {
                    await steps.daysTillNextBirthdate(contextMock);
                    expect(contextMock.sendText).toHaveBeenCalledWith('Goodbye👋');
                });
            });

            describe('otherwise', () => {
                beforeEach(() => {
                    contextMock.event.text = 'sadassadas';
                });

                it('should send "Goodbye👋"', async () => {
                    await steps.daysTillNextBirthdate(contextMock);
                    expect(contextMock.sendText).toHaveBeenCalledWith('Goodbye👋');
                });
            });
        });
    });
});
