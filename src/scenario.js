const steps = require('./steps');
const {messagesService} = require('./messages-service');

const scenario = [
    async (context) => {
        await context.sendText('Hi');
        context.setState({
            step: 1,
        });
    },
    steps.usersName,
    steps.birthdate,
    steps.daysTillNextBirthdate,
    steps.repeat,
];

async function scenarioRunner(context) {
    if (!context.event.isText) {
        await steps.useTextMessage();
        return;
    }

    messagesService.add(context.event.text);

    let currentStep;
    do {
        currentStep = context.state.step;
        await scenario[currentStep](context);
    } while(currentStep !== context.state.step);
}

module.exports = {
    scenario, 
    scenarioRunner
};